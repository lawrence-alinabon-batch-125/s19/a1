/*
Graded ACtivity: s19 a1

Create a new set of pokemon for pokemon battle.
Solve the health of the pokemon when tackle is invoked, current value of health should decrease.

if health is now below 10, invoke faint function

*/

function Pokemon(name, lvl, hp){
	this.name = name;
	this.level = lvl;
	this.health = hp * 2;
	this.attack = lvl;
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`);
		target.health = target.health - this.attack;
		console.log(`${target.name}'s health is now reduced to ${target.health}`);

		if(target.health < 10){
			 console.log(this.faint());
		}
		return ``;

	};
	this.faint = function(){
		return `${this.name} fainted`;
	}
}

let Wobbuffet = new Pokemon(`Wobbuffet`, 31, 85);
let Eevee = new Pokemon(`Eevee`, 31 , 80);

console.log(Wobbuffet.tackle(Eevee));
console.log(Wobbuffet.tackle(Eevee));
console.log(Wobbuffet.tackle(Eevee));
console.log(Wobbuffet.tackle(Eevee));
console.log(Wobbuffet.tackle(Eevee));
